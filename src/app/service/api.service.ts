import { Injectable } from "@angular/core";
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse,
} from "@angular/common/http";
import { catchError, Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class ApiService {
  baseUri: string = "http://localhost:3000/api";
  headers = new HttpHeaders().set("Content-Type", "application/json");
  constructor(private http: HttpClient) {}
  // Get all cities
  getCities() {
    return this.http.get(`${this.baseUri}`);
  }
  //Create City
  createCity(data): Observable<any> {
    let url = `${this.baseUri}/create`;
    return this.http.post(url, data);
  }
  //delete City
  deleteCity(id): Observable<any> {
    let url = `${this.baseUri}/delete/${id}`;
    return this.http.delete(url, { headers: this.headers });
  }
  //Get Customers
  getCustomers() {
    return this.http.get(`${this.baseUri}/GetCustomers`);
  }
  createCustomer(data): Observable<any> {
    let url = `${this.baseUri}/createCustomer`;
    return this.http.post(url, data);
  }
}

import { ChangeDetectorRef, Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ModalDismissReasons, NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ApiService } from "src/app/service/api.service";

@Component({
  selector: "app-company",
  templateUrl: "./company.component.html",
  styleUrls: ["./company.component.scss"],
})
export class CompanyComponent implements OnInit {
  companyForm: FormGroup;
  submitted = false;
  constructor(
    private apiService: ApiService,
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private cd: ChangeDetectorRef
  ) {}
  ngOnInit(): void {
    this.companyForm = this.formBuilder.group({
      name: ["", Validators.required],
      PhoneNumber: ["", Validators.required],
      email: ["", Validators.required],
      Address: [""],
    });
  }
  get f() {
    return this.companyForm.controls;
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.companyForm.invalid) {
      return;
    } else {
      // this.apiService.createCity(this.cityForm.value).subscribe(
      //   (res) => {
      //     console.log("City successfully created!");
      //     this.getCities();
      //     this.refresh();
      //   },
      //   (error) => {
      //     console.log(error);
      //   }
      // );
    }
  }
}

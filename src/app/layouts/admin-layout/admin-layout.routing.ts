import { Routes } from "@angular/router";

import { DashboardComponent } from "../../pages/dashboard/dashboard.component";
import { UserProfileComponent } from "../../pages/user-profile/user-profile.component";
import { CityComponent } from "src/app/pages/city/city.component";
import { ProductComponent } from "src/app/pages/product/product.component";
import { CompanyComponent } from "src/app/pages/company/company.component";
import { CustomerComponent } from "src/app/pages/customer/customer.component";

export const AdminLayoutRoutes: Routes = [
  { path: "dashboard", component: DashboardComponent },
  { path: "user-profile", component: UserProfileComponent },
  { path: "city", component: CityComponent },
  { path: "product", component: ProductComponent },
  { path: "company", component: CompanyComponent },
  { path: "customer", component: CustomerComponent },
];

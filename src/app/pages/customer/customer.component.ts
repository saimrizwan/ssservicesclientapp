import { ChangeDetectorRef, Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ModalDismissReasons, NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ApiService } from "src/app/service/api.service";
import Swal from "sweetalert2";
@Component({
  selector: "app-customer",
  templateUrl: "./customer.component.html",
  styleUrls: ["./customer.component.scss"],
})
export class CustomerComponent implements OnInit {
  Cities: any = [];
  customerForm: FormGroup;
  closeModal: string;
  Customers: any = [];
  submitted = false;
  constructor(
    private apiService: ApiService,
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private cd: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    this.customerForm = this.formBuilder.group({
      Name: ["", Validators.required],
      ShortName: ["", Validators.required],
      ContactPerson: ["", Validators.required],
      ContactPersonPhoneNumber: ["", Validators.required],
      Email: ["", Validators.required],
      cityId: ["", [Validators.required]],
      Address: [""],
    });
    this.getCities();
    this.getCustomers();
  }
  get cityId() {
    return this.customerForm.get("cityId");
  }
  getCities() {
    this.apiService.getCities().subscribe((res) => {
      this.Cities = res;
    });
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.customerForm.invalid) {
      return;
    } else {
      this.apiService.createCustomer(this.customerForm.value).subscribe(
        (res) => {
          Swal.fire({
            title: "Success!",
            text: "Customer Added",
            icon: "success",
            confirmButtonText: "Ok",
          });
          this.getCustomers();
          this.refresh();
          this.customerForm.reset();
          this.customerForm.markAsTouched();
          this.modalService.dismissAll();
        },
        (error) => {
          Swal.fire({
            title: "Error!",
            text: "Something Went Wrong!",
            icon: "error",
            confirmButtonText: "Ok",
          });
        }
      );
    }
  }
  refresh() {
    this.cd.detectChanges();
  }
  changeCity(e) {
    this.cityId.setValue(e.target.value, {
      onlySelf: true,
    });
  }
  getCustomers() {
    this.apiService.getCustomers().subscribe((res) => {
      this.Customers = res;
    });
  }
  triggerModal(content) {
    this.modalService
      .open(content, { ariaLabelledBy: "modal-basic-title" })
      .result.then(
        (res) => {
          this.closeModal = `Closed with: ${res}`;
        },
        (res) => {
          this.closeModal = `Dismissed ${this.getDismissReason(res)}`;
        }
      );
  }
  get f() {
    return this.customerForm.controls;
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return "by pressing ESC";
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return "by clicking on a backdrop";
    } else {
      return `with: ${reason}`;
    }
  }
}

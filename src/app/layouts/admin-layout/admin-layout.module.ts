import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";
import { RouterModule } from "@angular/router";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { ClipboardModule } from "ngx-clipboard";

import { AdminLayoutRoutes } from "./admin-layout.routing";
import { DashboardComponent } from "../../pages/dashboard/dashboard.component";
import { UserProfileComponent } from "../../pages/user-profile/user-profile.component";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { CityComponent } from "src/app/pages/city/city.component";
import { ApiService } from "src/app/service/api.service";
import { ProductComponent } from "src/app/pages/product/product.component";
import { CompanyComponent } from "src/app/pages/company/company.component";
import { CustomerComponent } from "src/app/pages/customer/customer.component";
// import { ToastrModule } from 'ngx-toastr';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgbModule,
    ClipboardModule,
  ],
  providers: [ApiService],
  declarations: [
    DashboardComponent,
    UserProfileComponent,
    CityComponent,
    ProductComponent,
    CompanyComponent,
    CustomerComponent,
  ],
})
export class AdminLayoutModule {}

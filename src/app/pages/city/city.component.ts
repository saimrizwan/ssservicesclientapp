import { ChangeDetectorRef, Component, OnInit } from "@angular/core";
import { ApiService } from "src/app/service/api.service";
import { ModalDismissReasons, NgbModal } from "@ng-bootstrap/ng-bootstrap";
import {
  FormGroup,
  FormControl,
  FormBuilder,
  Validators,
} from "@angular/forms";
import Swal from "sweetalert2";
@Component({
  selector: "app-city",
  templateUrl: "./city.component.html",
  styleUrls: ["./city.component.scss"],
})
export class CityComponent implements OnInit {
  cityForm: FormGroup;
  Cities: any = [];
  closeModal: string;
  submitted = false;
  constructor(
    private apiService: ApiService,
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private cd: ChangeDetectorRef
  ) {}
  ngOnInit(): void {
    this.cityForm = this.formBuilder.group({
      name: ["", Validators.required],
    });
    this.getCities();
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.cityForm.invalid) {
      return;
    } else {
      this.apiService.createCity(this.cityForm.value).subscribe(
        (res) => {
          Swal.fire({
            title: "Success!",
            text: "City Added",
            icon: "success",
            confirmButtonText: "Ok",
          });
          this.getCities();
          this.refresh();
          this.cityForm.reset();
          this.cityForm.markAsTouched();
          this.modalService.dismissAll();
        },
        (error) => {
          Swal.fire({
            title: "Error!",
            text: "Something Went Wrong!",
            icon: "error",
            confirmButtonText: "Ok",
          });
        }
      );
    }
  }
  removeCity(id) {
    Swal.fire({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then((result) => {
      if (result.isConfirmed) {
        this.apiService.deleteCity(id).subscribe((data) => {
          Swal.fire("Deleted!", "Your file has been deleted.", "success");
          this.getCities();
          this.refresh();
        });
      }
    });
  }
  get f() {
    return this.cityForm.controls;
  }
  getCities() {
    this.apiService.getCities().subscribe((res) => {
      this.Cities = res;
    });
  }
  triggerModal(content) {
    this.modalService
      .open(content, { ariaLabelledBy: "modal-basic-title" })
      .result.then(
        (res) => {
          this.closeModal = `Closed with: ${res}`;
        },
        (res) => {
          this.closeModal = `Dismissed ${this.getDismissReason(res)}`;
        }
      );
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return "by pressing ESC";
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return "by clicking on a backdrop";
    } else {
      return `with: ${reason}`;
    }
  }
  refresh() {
    this.cd.detectChanges();
  }
}

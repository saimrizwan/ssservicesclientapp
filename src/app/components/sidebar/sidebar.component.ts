import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

declare interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
}
export const ROUTES: RouteInfo[] = [
  {
    path: "/dashboard",
    title: "Dashboard",
    icon: "ni-tv-2 text-yellow",
    class: "",
  },
  {
    path: "/city",
    title: "City",
    icon: "ni-air-baloon text-pink",
    class: "",
  },
  {
    path: "/product",
    title: "Product",
    icon: "ni-square-pin text-primary",
    class: "",
  },
  {
    path: "/company",
    title: "Company",
    icon: "ni-tv-2 text-primary",
    class: "",
  },
  {
    path: "/customer",
    title: "Customer",
    icon: "ni-tag text-primary",
    class: "",
  },
  {
    path: "/user-profile",
    title: "User profile",
    icon: "ni-single-02 text-yellow",
    class: "",
  },
];

@Component({
  selector: "app-sidebar",
  templateUrl: "./sidebar.component.html",
  styleUrls: ["./sidebar.component.scss"],
})
export class SidebarComponent implements OnInit {
  public menuItems: any[];
  public isCollapsed = true;

  constructor(private router: Router) {}

  ngOnInit() {
    this.menuItems = ROUTES.filter((menuItem) => menuItem);
    this.router.events.subscribe((event) => {
      this.isCollapsed = true;
    });
  }
}
